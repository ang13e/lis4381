> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Alexis Gayda

### A5 # Requirements:

#### README.md file should include the following items:

- Course title, your name, assignment requirements, as per A1;  
- Screenshots as per below examples;  
- Link to local lis4381 web app: http://localhost/repos/lis4381/  

Deliverables:
1. Provide Bitbucket read-only access to lis4381 repo (Language PHP), include links
to the other assignment repos you created in README.md, using Markdown syntax  
(README.md must also include screenshots as per above.)
2. Blackboard Links: lis4381 Bitbucket repo   




#### Assignment Screenshots:


*Screenshot of index*:

![Passed](img/index.png)

*Screenshot of error*:

![Failed](img/error.png)
