# LIS 4381 - Mobile Web Application Development

## Alexis Gayda

### LIS4381 Requirements:

*Course Work Links*

1. [A1 README.md](a1/README.md "My A1 README.md file")  
- Install AMPPS  
- Install JDK  
- Install Android Studio and create My First App  
- Provide screenshots of installations  
- Create Bitbucket repo  
- Complete Bitbucket tutorials  
- Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")  
- Complete healthy recipes Android app  
- Provide screenshots of completed app  

3. [A3 README.md](a3/README.md "My A3 README.md file")  
- Course title, your name, assignment requirements, as per A1  
- Screenshot of ERD  
- Screenshot of running application’s first user interface  
- Screenshot of running application’s second user interface  
- Links to the following files:  
1. a3.mwb  
2. a3.sql  

4. [A4 README.md](a4/README.md "My A4 README.md File")  
- Course title, your name, assignment requirements, as per A1;  
- Screenshots as per below examples;  
- Link to local lis4381 web app: http://localhost/repos/lis4381/  

Deliverables:  
1. Provide Bitbucket read-only access to lis4381 repo, include links to the other
assignment repos you created in README.md, using Markdown syntax  
(README.md must also include screenshots as per above.)  
2. Blackboard Links: lis4381 Bitbucket repo  
3. *Note*: the carousel *must* contain (min. 3) slides that either contain text or
images that link to other content areas marketing/promoting your skills.  

4. [P1 README.md](p1/README.md "My P1 README.md file")  
- Course title, your name, assignment requirements, as per A1  
- Screenshot of running application’s first user interface  
- Screenshot of running application’s second user interface  

5. [A5 README.md](a5/README.md "My A5 README.md file")  
- Screenshot of index.php  
- Screenshot of error page for server side validation  

6. [P2 README.md](p2/README.md "My P2 README.md file")  
- Screenshot of index.php  
- Screenshot of error page for server side validation
- Screenshot of edit feature   
