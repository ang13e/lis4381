# LIS 4381

## Alexis Gayda

### A2 # Requirements:
1. Screenshot of running application’s first user interface;
2. Screenshot of running application’s second user interface;


#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of running application’s first user interface;
* Screenshot of running application’s second user interface;



#### Assignment Screenshots:

*Screenshot of running application's first user interface*:

![First User Interface](img/first_interface.png)

*Screenshot of running application's second user interface*:

![Second User Interface](img/second_interface.png)
