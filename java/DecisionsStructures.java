import java.util.Scanner;
class DecisionsStructures
{
  public static void main(String args [])
  {



char myChar=' ';
Scanner sc = new Scanner (System.in);


System.out.println("Phone types: W or w (Work), C or c (cell), H or h (home), N or n (none.)");
System.out.print("Enter phone type: ");
myChar = Character.toLowerCase(sc.next().charAt(0));

System.out.println("\nIf..else:");

if (myChar == 'w')
    System.out.println("Phone Type: work");
  else if (myChar == 'c')
    System.out.println("Phone type: cell");
  else if (myChar == 'h')
    System.out.println("Phone type: home");
  else if (myChar ==  'n')
  System.out.println("Phone type: none");
  else
  System.out.println("Incorrect character entry.");

System.out.println(); //blank line

System.out.println("switch:");
switch (myChar)
{
  case 'w':
  System.out.println("Phone type: work");
  break;
  case 'c':
  System.out.println("Phone type: cell");
  break;
  case 'h':
  System.out.println("Phone type: home");
  break;
  case 'n':
  System.out.println("Phone type: none");
  break;
  default:
  System.out.println("Incorrect character entry.");
  break;
}

}
}
