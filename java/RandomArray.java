import java.util.*;
class RandomArray
{
  public static void main(String[] args)
  {
  Random r = new Random();
  int arraySize = 0;
  int i = 0;
  Scanner sc = new Scanner(System.in);

  System.out.println("Enter desired number of pseudoranom-generated integers (min1): ");
  arraySize = sc.nextInt();

  int myArray[] = new int[arraySize];

  //for loops
  System.out.println("For loop: ");
    for(i=0; i<myArray.length; i++)
    {
      System.out.println(r.nextInt());

    }
    //Enhanced for loops
    System.out.println("\nEnhanced for loop:");
    for(int n: myArray)
    {
      System.out.println(r.nextInt());
    }

    //While loop
    System.out.println("\nWhile loop: ");
    i=0;
    while (i<myArray.length)
    {
      System.out.println(r.nextInt());
      i++;
    }
    //do..while loop
    System.out.println("\nDo..While loop:");
    i=0;
    do{
      System.out.println(r.nextInt());
      i++;
    }
    while(i<myArray.length);

  }
}
