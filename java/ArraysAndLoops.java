import java.util.Scanner;

class ArraysAndLoops
{
   public static void main(String args[])
   {
     //Display operational messages
System.out.println("Program loops through array of strings.");


     String animals [] = {"dog", "cat", "bird", "fish", "insect"};

System.out.println("\nFor loop:");
     for (int i = 0; i < animals.length; i++)
     {
             System.out.println(animals[i]);
         }

System.out.println("\nEnhanced for loop:");
         for (String test : animals) {
             System.out.println(test);
         }
System.out.println("\nWhile Loop:");
  int i=0;
  while (i < animals.length)
  {
    System.out.println(animals[i]);
    i++;
  }

  i=0;
  System.out.println("\nDo..While Loop:");
  do
  {
    System.out.println(animals[i]);
    i++;
  }
  while (i < animals.length);


}
}
