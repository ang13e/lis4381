import java.util.Scanner;
import java.util.Random;

class Method
{
  private static int myArray[];

  public static int randomFill()
  {
    System.out.println("Inside randomFill().");
    Random r = new Random();
    int randomNum = r.nextInt(10) + 1;
    return randomNum;
  }

  public static int[] arrayFill(int arrayNum)
  {
    System.out.println("Inside arrayFill().");
    myArray = new int[arrayNum];
    for(int i=0; i < myArray.length; i++)
    {
      myArray[i] = randomFill();
    }
    return myArray;
  }

  public static void print()
  {
    System.out.println("Inside print().");
      System.out.println("\nEnhanced for loop:");
      for(int n: myArray)
      {
        System.out.println(n);
      }
      System.out.println("\nforloop:");
      for(int i=0; i< myArray.length; i++)
      {
        System.out.println(myArray[i]);
      }
      System.out.println("\nwhile loop:");
      int i=0;
      while (i < myArray.length)
      {
        System.out.println(myArray[i]);
        i++;
      }

      i=0;

      System.out.println("\ndo...while loop:");
      do {
        {
          System.out.println(myArray[i]);
          i++;
        }
      } while (i < myArray.length);
  }

  public static void main(String args[])
  {
    System.out.println("Program prompts user to enter desired number of pseudoranom-generated integers (min1).");
    System.out.println();

    Scanner sc = new Scanner (System.in);
    int arrayLength=0;

    System.out.print("Enter desired number of pseudoranom-generated integers: ");
    arrayLength = sc.nextInt();
    System.out.println("Call arrayFill().");
    arrayFill(arrayLength);

    System.out.println("\nCall print().");
    print();
  }

}
