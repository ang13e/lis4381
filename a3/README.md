> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Alexis Gayda

### A3 # Requirements:

#### README.md file should include the following items:

1. Course title, your name, assignment requirements, as per A1  
2. Screenshot of ERD  
3. Screenshot of running application’s first user interface  
4. Screenshot of running application’s second user interface  
5. Links to the following files:  
- a3.mwb  
- a3.sql




#### Assignment Screenshots:

*Screenshot of ERD*:

![Screenshot of ERD](img/a3.png)

*Screenshot of running first user interface*:

![First user interface](img/ui1.png)

*Screenshot of running second user interface*:

![Second user interface](img/ui2.png)


####Links:

*a3.mwb*
[a3.mwb](docs/a3.mwb/ "A3.mwb")

*a3.sql*
[a3.sql](docs/a3.sql/ "A3.sql")
