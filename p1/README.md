> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Alexis Gayda

### P1 # Requirements:

#### README.md file should include the following items:

1. Course title, your name, assignment requirements, as per A1  
2. Screenshot of running application’s first user interface  
3. Screenshot of running application’s second user interface  



#### Assignment Screenshots:

*Screenshot of running first user interface*:

![First user interface](img/ui1.png)

*Screenshot of running second user interface*:

![Second user interface](img/ui2.png)
